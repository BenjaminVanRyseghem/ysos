# Ysos

## Language Features

  - Object oriented
  - Homoiconic syntax
  - Immutable
  - Runs on top of JS

## License

  - Copyright 2014 under the license GPL v3
  - Authors:
      - Nicolas Petton <nicolas [dot] petton [at] gmail [dot] com>
      - Benjamin Van Ryseghem <benjamin [dot] vanryseghem [at] gmail [dot] com>

## Syntax examples
	self method (
		foo(a b) (
			@a + b
		)
	);

	Data: #(method foo (a b) (a + 2) (a + b))

	macro bar (a b) (
		(self foo a b)
	);

	lambda (a b) (
		(self foo a b)
	);
	-> a Closure

	{
		a: 2,
		b: 3
	}


	Object subclass (
		'Foo' (
			self initialize(a) (
				@a = a
			);

			self method (
				foo(a b) (
					@a + b
				)
			);

			self method bar(a b) (
				@a - b
			);

			self method at(a ifNil: (self error) ifNotNil: blabla: 2) (
				...
			);
		);
	);


## Transaction idea for implementation

    #foo alter (#())
    alter(lambda, env){
    	var old = env.revolve(this.$content);
		var result = lambda();
		if(env.revolve(this.$content) === old){
			env[this.$content] = result;
		} else{
			return alter(lambda, env);
		}
		return result;
    }



