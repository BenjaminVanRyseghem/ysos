requirejs.config({
	baseUrl: 'js',
	paths:   {
		'jQuery': 'lib/jQuery/jquery-2.1.1.min'
	}
});

// Start the main app logic.
requirejs(['loadYsos', 'bootstrap'],
	function(loadYsos, namespace) {
		loadYsos({
			whenLoadedCallback: function() {
				namespace;
				namespace.Dog.$new(35, 46, 'German shepard');
				debugger;
			}
		})
	});