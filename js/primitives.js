define([
], function() {

	var primitives = {};

	var klassClass = null;
	var namespace = null;

	var symbols = {};

	//
	// SETTERS
	//

	primitives.setKlassClass = function(newValue) {
		klassClass = newValue;
	};

	primitives.setNamespace = function(newValue) {
		namespace = newValue;
	};

	//
	// Primitives
	//

	primitives.inheritConstructors = function(spec) {
		var child = spec.child;

		var parent = spec.parent;
		child.prototype = new parent();
		child.prototype.constructor = child;
	};

	function instantiateNewClass(spec) {
		var superclass = spec.superclass || namespace["Object"];
		var superclassKlass;
		if (superclass === namespace['nil']) {
			superclassKlass = namespace['Class'];
		} else {
			superclassKlass = superclass.klass;
		}
		var metaClass = new klassClass();
		metaClass.isMeta = true;

		if (!spec.metaBuilder) {
			metaClass.builder = function() {
			};

			// Set up the parallel hierarchy
			metaClass['@superclass'] = superclassKlass;
			primitives.inheritConstructors({
				child:  metaClass.builder,
				parent: superclassKlass.builder
			})

		} else {
			metaClass.builder = spec.metaBuilder;
		}

		var newClass = new metaClass.builder();
		newClass.klass = metaClass;
		metaClass.instance = newClass;
		return newClass;
	}

	primitives.defineClass = function(spec) {
		var superBuilder;

		if (namespace[spec.name] && (namespace[spec.name]['@superclass'] === (spec.superclass || namespace["Object"]))) {
			namespace[spec.name].__source = window.currentFilePath;
			return namespace[spec.name];
		}

		var klass = instantiateNewClass(spec);

		klass.builder = spec.builder || function() {
		};

		klass['@superclass'] = spec.superclass || namespace["Object"];
		if (klass['@superclass'] && klass['@superclass'].__subclasses) {
			klass['@superclass'].__subclasses.push(klass);
		}

		klass.wrapped = spec.builder || false;

		if (klass['@superclass'] === namespace['nil']) {
			superBuilder = function() {
			};
		} else {
			superBuilder = klass['@superclass'].builder;
		}

		if (!klass.wrapped) {
			primitives.inheritConstructors({
				child:  klass.builder,
				parent: superBuilder
			})
		} else {
			klass.builder.prototype.__isYsosObject = true;
		}

		klass.builder.prototype.klass = klass;

		klass.name = spec.name;
		klass.__subclasses = [];
		klass.__methodDictionary = {};
		klass.__classMethodDictionary = {};
		klass.__source = window.currentFilePath;

		namespace[ klass.name ] = klass;

		return klass;
	};

	primitives.defineMethod = function(spec) {
		spec.class.builder.prototype['$' + spec.name] = spec.fn;

		spec.fn.hostClass = spec.class;
		spec.fn.ysosArguments = spec.arguments;
		spec.fn.source = window.currentFilePath;

		if (spec.special) {
			spec.fn.specialMethod = true;
		}

		if (spec.noArgumentsSeparation) {
			spec.fn.noArgumentsSeparation = true;
		}

		if (!spec.class.isMeta) {
			spec.class.__subclasses.forEach(function(each) {
				if (each.wrapped) {
					var options = Object.create(spec);
					options.class = each;
					primitives.defineMethod(options);
				}
			});
		}
	};

	primitives.resolveMethod = function(receiver, prefix, selector, superCall, lookupStartingPoint) {
		var method = receiver[ prefix + selector ];
		if (method === undefined) {
			return undefined;
		}

		if (superCall) {
			method = lookupStartingPoint.builder.prototype[ prefix + selector ];
		}

		return method;
	};

	primitives.sendMessage = function(spec) {
		var receiver = spec.receiver;
		var selector = spec.selector;
		var arguments = spec.arguments;
		var currentEnvironment = spec.currentEnvironment;
		var startingPoint = spec.startingPoint;
		var superCall = spec.superCall;
		var jsCall = spec.jsCall;

		var argumentsWithEnvironment = Object.create(arguments);
		argumentsWithEnvironment.push(currentEnvironment);
		argumentsWithEnvironment.push(startingPoint);

		var prefix = jsCall ? '' : '$';

		var method = primitives.resolveMethod(receiver, prefix, selector.__content, superCall, startingPoint);

		if (method === undefined) {
			var messageSend = new namespace["MessageSend"].builder();
			messageSend['@selector'] = selector;
			messageSend['@arguments'] = arguments;
			messageSend['@superCall'] = superCall;
			return receiver['$messageNotFound'].apply(receiver, [messageSend, currentEnvironment]);
		}

		return method.apply(receiver, argumentsWithEnvironment);
	};

	primitives.newSymbol = function(content) {
		if (symbols[content] === undefined) {
			var newSymbol = new namespace['Symbol'].builder();
			newSymbol.__content = content;
			symbols[content] = newSymbol;
		}

		return symbols[content];
	};

	primitives.wrapJavascript = function(object) {
		var result;

		if (object == undefined) {
			return namespace['nil'];
		}

		if (object.__isYsosObject) {
			return object;
		}

		if (object.constructor === Array) {
			var array = new namespace['Array'].builder();
			array.__content = object;
			return array;
		}

		if (jQuery && object.constructor === jQuery) {
			object.asArray = function() {
				var jQueryArray = new namespace['Array'].builder();
				var content = object.get();
				content = content.map(function(each) {
					return primitives.wrapJavascript(jQuery(each));
				});
				jQueryArray.__content = content;
				return jQueryArray;
			};
		}

		if (typeof object === 'function') {
			result = new namespace['JSFunction'].builder();
		} else {
			result = new namespace['JSProxy'].builder();
		}

		result.__content = object;
		return result;
	};

	return primitives;
});