define([
	'./parser'
], function(parse) {
	function Compiler() {
	}

	/** anArray is the body on the method,
	 * so it's an array of arrays.
	 * anArray item represents a sequence.
	 */
	Compiler.prototype.compile = function(anArray) {
		var that = this;

		return anArray.map(function(sequence) {
			return that.compileSequence(sequence);
		}).join(';\n');
	};

	/**
	 * anArray represents a sequence
	 *
	 * Example:
	 *    self foo(11) bar(2333);
	 *    -> (self foo (11) bar (2333))
	 *  or [ "self", "foo", [ 11 ], "bar", [ 2333 ] ]
	 */
	Compiler.prototype.compileSequence = function(anArray) {
		if (anArray.length === 1){
			return anArray[0] + ';\n';
		}

		var result = '';
		var that = this;
		var receiver = anArray[0];
		var selector = anArray[1];
		var args = anArray[2];
		var left = anArray.splice(3, anArray.length);

		result += receiver;

		while (left.length > 0) {

			result += '.';
			result += selector;
			result += that.compileArguments(args);

			selector = left[0];
			args = left[1];
			left = left.splice(2, left.length);
		}

		result += '.';
		result += selector;
		result += that.compileArguments(args);

		return result;
	};

	Compiler.prototype.compileArguments = function(anArray) {
		var that = this;

		var map = anArray.map(function(each) {
			return that.compileArgument(each);
		});

		return '(' + map.join(', ') + ')';
	};

	Compiler.prototype.compileArgument = function(anArgument) {
		var that = this;
		if(anArgument.constructor !== Array){
			return anArgument;
		}

		return '(' + anArgument.map(function(each) {
			return that.compileArgument(each);
		}).join(', ') +')';
	};

	var compiler = new Compiler;

//	var ast = [
//		[
//			"self",
//			"foo",
//			[ 11, 24 ],
//			"bar",
//			[ 2333 ]
//		],
//		[
//			"self",
//			"baz",
//			[]
//		],
//		[
//			12
//		]
//	];

	var ast = parse.parse('Object subclass("Foo" ( self foo () ) );');
	console.log(compiler.compile(ast));
});

// Object subclass("Foo"(	self initialize(a)(	@a = a ); self method( 	foo(a b ) (	@a + b 	) ) ; self method bar(a b )( @a - b  ); self method at(a ifNil: (self error ) ifNotNil: blabla: 2) () ; ) ;) ;