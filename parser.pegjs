start					= block / list / literal / listWithParenthesis / comments

separator				= [ \t\v\f\u00A0\uFEFF\n\r\u2028\u2029]+

lineComment				= separator* "//" [^\n\r\u2028\u2029]* [\n\r\u2028\u2029]?
blockComment			= "\/\*" [^\/\*]* "\*\/"

comments				= comment* {
							return undefined;
						}

comment					= lineComment / blockComment

ws						= (comment / separator)*

symbol					= chars:[a-zA-Z0-9\+_\-\`\|\&\/\=\?\!\<\@\*\#\>]+ {
							return primitives.newSymbol(chars.join(""));
						}
						/ ':=' {
							return primitives.newSymbol(':=');
						}

blockSymbol				= chars:[a-zA-Z]+ extra:([0-9\+_\-\|\&\=\?\!\<\@\*\#\:\>]+[a-zA-Z]+)? {
							var characters = chars.join("");
							if(extra){
								characters += extra[0].join('')+extra[1].join('');
							}

							return primitives.newSymbol(characters);
						}

stringSingleQuote		= ['] val:(("\\'" {return "'";} / [^'])*) ['] {
							return val.join("").replace(/\'/ig, "'");
						}

stringDoubleQuote		= ["] val:(('\\"' {return '"';} / [^"])*) ["] {
							return val.join("").replace(/\"/ig, '"');
						}

string					= stringSingleQuote / stringDoubleQuote


dictionaryBinding		= ws key:(string / symbol) ws ':' !('=') ws value: listItem ws {
							return {
								key: key,
								value: value
							};
						}
dictionary				= ws '{' ws '}' {
 							return new namespace['Dictionary'].builder();
						}
						/ ws '{' ws binding:dictionaryBinding ws '}' {
						 	var result = new namespace['Dictionary'].builder();
						 	result['__' + binding.key] = binding.value;
						 	return result;
						}
						/ ws '{' bindings:( dictionaryBinding ws ',' )* last:dictionaryBinding ws '}' ws {
							var result = new namespace['Dictionary'].builder();

							bindings = bindings.map(function(each){
								return each[0];
							});
							bindings.forEach(function(binding){
								result['__' + binding.key] = binding.value;
							});
							result['__' + last.key] = last.value;

							return result;
						}

literal					= number
						/ binding:dictionaryBinding {
							var result = new namespace['ArgumentAssociation'].builder();
							result['@key'] = binding.key;
							result['@value'] = binding.value;
							return result;
						}
						/ literalList
						/ symbol
						/ string
						/ dictionary

number					= sign: '-'? digits:[0-9]+ {
						if(sign) {
							return parseInt('-'+digits.join(''), 10);
						} else {
							return parseInt(digits.join(''), 10);
						}
					}

listSeparator			= ";"
argumentsSeparator		= ','

literalList				= '#' list:listWithParenthesis {
							var result = new namespace['Array'].builder();
							result.__content	= list;
							return result;
						}

listItem 				= listWithParenthesis / literal / block

listItemWithStatement	= st:statement {
							options.statement = true;
							st.isStatement = true;
							return st;
						}
						/ argumentsSeparator {
							return primitives.newSymbol(',');
						}
						/ l:literal {
							if(options.statement) {
								return [l]
							}
							return l;
						}
						/ list:listWithParenthesis{
							return list;
						}
						/ block

listWithParenthesis		= !{ options.statement = false; } ws "(" literals:(ws listItemWithStatement ws)* ws ")" {
							var result = [];
							var temp = [];
							var each;


							var firstResult = [];
							literals.forEach(function(each){
								var item = each[1];


								firstResult.push(item);
							});

							firstResult.forEach(function(each){
								if(each.isStatement){
									result.push(each);
									temp = [];
								} else {
									if(each.length){
										temp.push(each[0]);
									} else if(each.$__isSymbol) {
										temp.push(each);
									} else {
										temp.push([]);
									}
								}
							});
							
							if(temp.length){
								result.push(temp);
							}
							
							if(!options.statement){
								result = firstResult;
							}

							if(literals.length){
								options.statement = false;
							}

							return result;
						}

statement 				= items:(listItem ws)+ listSeparator ws {
							return items.map(function(each){
								return each[0];
							});
						}

block 					= ws '[' ws args:((blockSymbol ws)+ ws '->' ws)? b:list?
']' {
							var arguments = new namespace['Array'].builder();
							arguments.__content = [];

							if(args) {
								arguments.__content = args[0].map(function
								(each){
									return each[0];
								});
							}

							var body = new namespace['Array'].builder();
							body.__content = b || [];

							var result = new namespace['Lambda'].builder();

							result['@arguments'] = arguments;
							result['@body'] = body;

							return result;
						}

list					= ws elements:(statement)+ others:((listItem ws)+)? {
							var result 	= elements;
							if(others){
								result.push(others.map(function(each){
									return each[0];
								}));
							}
							return result;
						}
						/ ws items:(listItem ws)+ ws {
							return [ items.map(function(each){
								return each[0]
							})];
						}