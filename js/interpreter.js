define([
	'./bootstrap',
	'./primitives'
], function(namespace, primitives) {

	primitives.environment = environment;
	primitives.extractArguments = extractArguments;

	function environment(parent) {
		var that = {};
		that.parentEnvironment = parent;

		if (that.parentEnvironment) {
			that.variables = {};
		} else {
			that.variables = Object.create(namespace);
		}
		that.variables.self = namespace["nil"];
		that.variables.super = namespace["nil"];

		that.resolve = function(aName, preventCheckOnWindow) {
			if (aName[0] === '@' && that.variables['self'][aName] !== undefined) {
				return that.variables['self'][aName];
			}

			if (that.parentEnvironment) {
				var result = that.parentEnvironment.resolve(aName, true);
				if (result !== namespace['nil']) {
					return result;
				}
			}

			if (that.variables[aName] !== undefined) {
				return that.variables[aName];
			}

			if (!preventCheckOnWindow && window[aName] !== undefined) {
				return primitives.wrapJavascript(window[aName]);
			}

			return namespace['nil'];
		};

		that.assign = function(spec) {
			var name = spec.name;
			if (name[0] === '@') {
				var receiver = that.resolve('self');
				if (receiver.immutable) {
					throw 'Inst var ("' + name + '") can only be set during initialization';
				}
				that.resolve('self', true)[name] = spec.value;
			} else {
				if (that.resolve(name, true) !== undefined && that.resolve(name, true) !== namespace['nil']) {
					throw 'Can not mutate variable "' + name + '"';
				}
				that.variables[name] = spec.value;
			}
		};

		that.forceAssign = function(spec, isParent) {
			var name = spec.name;

			if (name[0] === '@') {
				that.resolve('self', true)[name] = spec.value;
				return true;
			}

			if (that.parentEnvironment) {
				var result = that.parentEnvironment.forceAssign(spec, true);
				if (result) {
					return true;
				}
			}

			if (that.variables[name] !== undefined) {
				that.variables[name] = spec.value;
				return true;
			}

			if (isParent) {
				return false;
			}
			that.variables[name] = spec.value;
			return true;
		};

		that.clone = function() {
			var clone = environment();

			clone.parentEnvironment = that.parentEnvironment;
			clone.variables = Object.create(that.variables);

			return clone;
		};

		that.equal = function(another) {
			var flag = true;
			var key;
			for (key in that.variables) {
				var each = that.variables[key];
				flag = flag && (each === another.variables[key]);
			}
			return flag;
		};

		return that;
	}

	function interpreter(anArray, currentEnvironment, lookupStartingPoint) {

		currentEnvironment = currentEnvironment || environment();

		function interpret(sequences) {
			var result = undefined;
			sequences = Object.create(sequences);

			sequences.forEach(function(each) {
				result = interpretMessageSend(each);
			});

			return result;
		}

		function computeCommaSeparatedArguments(firstElement, receiver, selector, left, superCall, startingPoint) {
			var arguments;
			var method = primitives.resolveMethod(receiver, '$', selector.__content, superCall, startingPoint);

			var methodArguments = retrieveArguments(method, receiver);
			var numberOfArguments;

			if (method) {
				if (methodArguments === null) {
					numberOfArguments = 3;
				}
				else {
					numberOfArguments = methodArguments.length;
				}
			} else {
				numberOfArguments = 3;
			}

			var noArgumentsSeparation = method && method.noArgumentsSeparation;

			switch (numberOfArguments) {
				case 0:
					if (firstElement !== undefined && firstElement.constructor === Array) {
						if (firstElement.length === 0) {
							// pop first element
							left.splice(0, 1);
							return [];
						} else {
							throw 'An empty array is expected as argument for "' + selector.__content + '"';
						}
					} else {
						return [];
					}
					break;
				case 1:
					if (firstElement === undefined) {
						throw 'A non empty array is expected as argument for "' + selector.__content + '"';
					}
					if (firstElement.constructor === Array) {
						if (noArgumentsSeparation) {
							arguments = firstElement;
						} else {
							arguments = extractArguments(firstElement);
						}

						if (arguments.length === 0) {
							throw 'A non empty array is expected as argument for "' + selector.__content + '"';
						} else {
							left.splice(0, 1);
							return arguments;
						}
					} else {
						return left.splice(0, 1);
					}
					break;
				default:
					if (firstElement !== undefined && firstElement.constructor === Array) {
						if (noArgumentsSeparation) {
							arguments = firstElement;
						} else {
							arguments = extractArguments(firstElement);
						}

						if (arguments.some(function(each) {
							return each.__isArgumentAssociation;
						})) {
							throw 'Impossible to mix keyword argument and comma separated arguments';
						}

						if (arguments.length === numberOfArguments) {
							left.splice(0, 1);
							return arguments;
						} else {
							if (!method || receiver.__isJSProxy || selector.__content == 'initialize' || (typeof method.ysosArguments === 'function')) {
								left.splice(0, 1);
								if (arguments.length === 1 && arguments[0].length === 0) {
									return [];
								} else {
									return arguments;
								}
							} else {
								throw 'The array provided as argument to "' + selector.__content + '" does not have the correct number of arguments. ' + numberOfArguments + ' expected, but ' + arguments.length + ' found';
							}
						}
					} else {
						if (method) {
							throw 'Array expected as argument to "' + selector.__content + '"';
						} else {
							return [];
						}
					}
					break;
			}
		}

		function retrieveArguments(method, receiver) {
			var args;

			if (!method) {
				return [];
			}

			if (typeof method.ysosArguments === 'function') {
				args = method.ysosArguments(receiver);
			} else {
				args = method.ysosArguments;
			}
			return args;
		}

		function computeKeywordArguments(firstElement, receiver, selector, left, superCall, startingPoint) {
			var method = primitives.resolveMethod(receiver, '$', selector.__content, superCall, startingPoint);

			// todo: this can be optimised later by just pushing variables in
			// the environment
			var args;
			var argumentsToCompute = extractArguments(firstElement);

			if (method.ysosArguments === null) {
				throw receiver.klass.name + '>>' + selector.__content + ' does not support keyword argument'
			}

			args = retrieveArguments(method, receiver);

			var result = [];
			for (var i = 0; i < args.length; i++) {
				// null is for default value
				result.push(null);
			}

			argumentsToCompute.forEach(function(each) {
				if (!each.__isArgumentAssociation) {
					throw 'Impossible to mix keyword argument and comma separated arguments';
				}
				var index = args.indexOf(each['@key'].__content);
				if (index !== -1) {
					result[index] = each['@value'];
				}
			});

			left.splice(0, 1);
			return result;
		}

		function computeArguments(receiver, selector, left, superCall, startingPoint) {
			var firstElement;
			firstElement = left[0];

			if (firstElement && firstElement[0] && firstElement[0].__isArgumentAssociation) {
				return computeKeywordArguments(firstElement, receiver, selector, left, superCall, startingPoint);
			} else {
				return computeCommaSeparatedArguments(firstElement, receiver, selector, left, superCall, startingPoint);
			}
		}

		function interpretMessageSend(aSequence) {
			var receiver;
			var left;

			if (aSequence.constructor !== Array) {
				return interpretLiteral(aSequence);
			}

			var superCall = aSequence[0].__content === 'super';

			aSequence = Object.create(aSequence);
			receiver = interpretLiteral(aSequence[0]);

			var startingPoint = lookupStartingPoint || receiver.klass;
			if (superCall) {
				startingPoint = startingPoint['@superclass'];
			}

			if (aSequence.length === 1) {
				return interpretLiteral(receiver);
			}

			var interpretedArgs;
			var selector = aSequence[1];
			var method = primitives.resolveMethod(receiver, '$', selector.__content, superCall, startingPoint);
			var specialMethod = method && method.specialMethod;

			left = aSequence.splice(2, aSequence.length);

			var args = computeArguments(receiver, selector, left, superCall, startingPoint);
			while (left.length > 0) {
				if (specialMethod) {
					interpretedArgs = args;
				} else {
					interpretedArgs = interpretArguments(args);
				}
				receiver = primitives.sendMessage({
					receiver:           receiver,
					selector:           selector,
					arguments:          interpretedArgs,
					startingPoint:      startingPoint,
					currentEnvironment: currentEnvironment,
					superCall:          superCall
				});
				superCall = false;
				startingPoint = receiver.klass;
				selector = left[0];
				left.splice(0, 1);
				method = primitives.resolveMethod(receiver, '$', selector.__content, superCall, startingPoint);
				specialMethod = method && method.specialMethod;
				args = computeArguments(receiver, selector, left, superCall, startingPoint);
			}

			if (specialMethod) {
				interpretedArgs = args;
			} else {
				interpretedArgs = interpretArguments(args);
			}
			return primitives.sendMessage({
				receiver:           receiver,
				selector:           selector,
				startingPoint:      startingPoint,
				arguments:          interpretedArgs,
				currentEnvironment: currentEnvironment,
				superCall:          superCall
			});
		}

		function unescape(literal) {
			if (literal.$__isSymbol) {
				if (literal.__content[0] === '`') {
					var symbol = primitives.newSymbol(literal.__content.substring(1, literal.__content.length));
					return resolveSymbol(symbol);
				} else {
					return literal;
				}
			}

			if (literal.$__isEscapedArray) {
				var result = new namespace['Array'].builder();
				result.__content = literal.__content.map(function(each) {
					return unescape(each);
				});
				return result;
			}

			if (literal.constructor === Array) {
				return literal.map(function(each) {
					return unescape(each);
				});
			}

			return literal;

		}

		function interpretArguments(args) {
			if (args.constructor !== Array) {
				return [interpreter([args], currentEnvironment)];
			}

			if (args.$__isEscapedArray) {
				var result = new namespace['Array'].builder();
				result.__content = unescape(args);

				return [result];
			}

			if (args.length === 0) {
				return [];
			}

			return args.map(function(each) {
				if (each.length === 0) {
					return [];
				}
				return interpreter([each], currentEnvironment);
			});
		}

		function interpretLiteral(receiver) {
			if (receiver.__isLambda) {
				var closure = new namespace['InterpretedClosure'].builder();
				closure['@arguments'] = receiver['@arguments'];
				closure['@body'] = receiver['@body'];
				closure['@environment'] = currentEnvironment;
				return closure;
			}

			if (receiver.constructor === Array) {

				// for the case ( (13 toString ()) chartAt (1) )
				return interpreter([receiver], currentEnvironment);
			}

			if (receiver.klass.name === 'Dictionary') {
				var dict = new namespace['Dictionary'].builder();
				for (var key in receiver) {
					if (key[0] === '_' && key[1] === '_') {
						dict[key] = interpret([receiver[key]]);
					}
				}
				return dict;
			}

			if (receiver.$__isEscapedArray) {
				return unescape(receiver);
			}

			if (receiver.$__isSymbol) {
				return resolveSymbol(receiver);
			}
			return receiver;
		}

		function resolveSymbol(aSymbol) {
			if (aSymbol.__content[0] === '#') {
				var content = aSymbol.__content;
				var name = content.substring(1, content.length);
				return primitives.newSymbol(name);
			}
			return currentEnvironment.resolve(aSymbol.__content);
		}

		return interpret(anArray);
	}

	function extractArguments(array) {
		function push(acc) {
			if (acc.length === 0) {
				return;
			}
			if (acc.length === 1) {
				result.push(acc[0]);
			} else {
				result.push(acc);
			}
		}

		var result = [];
		var temp = [];
		var separator = primitives.newSymbol(',');

		array.forEach(function(each) {
			if (each === separator) {
				push(temp);
				temp = [];
			} else {
				temp.push(each);
			}
		});
		push(temp);
		return result;
	}

	return interpreter;
});
