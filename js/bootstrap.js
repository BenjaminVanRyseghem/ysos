define([
	'./primitives'
], function(primitives) {

	/**
	 * Basic objects initialization
	 */

	var namespace = {};

	function YObject() {
	}

	function YObjectMetaClass() {

	}

	function YClass() {
	}

	/**
	 * primitives environment setting
	 */

	primitives.setNamespace(namespace);
	primitives.setKlassClass(YClass);

	/**
	 * Bootstrap relationships between Object, Class, and MetaClass
	 */

	primitives.inheritConstructors({
		child:  YClass,
		parent: YObject
	});

	primitives.inheritConstructors({
		child:  YObjectMetaClass,
		parent: YClass
	});

	primitives.defineClass({
		name:        'Object',
		builder:     YObject,
		bootstrap:   true,
		metaBuilder: YObjectMetaClass
	});

	primitives.defineClass({
		name:       'Class',
		builder:    YClass,
		bootstrap:  true,
		superclass: namespace["Object"]
	});

	namespace['Object'].klass['@superclass'] = namespace['Class'];

	YClass.prototype.klass = namespace["Class"];
	YClass.prototype.builder = function() {
	};
	YClass.prototype.builder.prototype = new YObject();
	YClass.prototype.builder.prototype['@superclass'] = namespace["Object"];

	/**
	 * Sets the method dictionary by hand since the classes
	 * has been created by hand.
	 */

	primitives.defineClass({
		name:      'Dictionary',
		bootstrap: true
	});

	namespace["Object"].__methodDictionary = new namespace['Dictionary'].builder();
	namespace["Object"].__classMethodDictionary = new namespace['Dictionary'].builder();
	namespace["Object"].builder.prototype.__isYsosObject = true;

	namespace["Class"].__methodDictionary = new namespace['Dictionary'].builder();
	namespace["Class"].__classMethodDictionary = new namespace['Dictionary'].builder();

	YClass.klass = namespace["Class"];

	var ysos = new namespace['Dictionary'].builder();
	ysos.__isNamespace = true;
	for (var each in namespace) {
		ysos[each] = namespace[each];
	}

	primitives.setNamespace(ysos);

	return ysos;
});