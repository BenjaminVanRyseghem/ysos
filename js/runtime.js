define([
	'./bootstrap',
	'./interpreter',
	'./primitives',
	'./primitivesTable'
], function(namespace, interpreter, primitives, primitivesTable) {

	/**
	 * Defines `new`
	 */

	primitives.defineMethod({
		name:      'initialize',
		arguments: [],
		fn:        function(currentEnvironment) {
		},
		class:     namespace["Object"]
	});

	primitives.defineMethod({
		name:      'new',
		arguments: function(receiver) {
			return receiver.$initialize.ysosArguments.length;
		},
		fn:        function() {

			var instance = new this.builder();
			var args = Array.prototype.slice.call(arguments);
			args[args.length - 1] = undefined;

			instance.$initialize.apply(instance, args);
			instance.immutable = true;
			return instance;
		},
		class:     namespace["Class"]
	});

	/**
	 * Defines `nil` and set it as `Object` superclass
	 */

	primitives.defineClass({name: 'UndefinedObject'});
	var nil = namespace["UndefinedObject"].$new();
	namespace["Object"]['@superclass'] = nil;
	namespace["nil"] = nil;

	/**
	 * Define `Symbol`
	 */
	primitives.defineClass({
		name: 'Symbol'
	});

	namespace['Symbol'].builder.prototype.__content = '';
	namespace['Symbol'].builder.prototype.$__isSymbol = true;

	/**
	 * Define `Array`
	 */

	primitives.defineClass({
		name: 'Array'
	});

	namespace['Array'].builder.prototype.__content = '';
	namespace['Array'].builder.prototype.$__isEscapedArray = true;

	/**
	 * Define `Boolean`
	 */

	primitives.defineClass({
		name:    'Boolean',
		builder: Boolean
	});

	namespace["true"] = true;
	namespace["false"] = false;

	primitives.defineMethod({
		name:      'new',
		arguments: [],
		fn:        function() {
			throw 'Boolean can not be instantiated';
		},
		class:     namespace["Boolean"].klass
	});

	/**
	 * Define Number
	 */

	primitives.defineClass({
		name:    'Number',
		builder: Number
	});

	/**
	 * Define String
	 */

	primitives.defineClass({
		name:    'String',
		builder: String
	});

	/**
	 * Define CompiledMethod
	 */

	primitives.defineClass({
		name: 'CompiledMethod'
	});

	/**
	 * Define ArgumentAssociation
	 */

	primitives.defineClass({
		name: 'ArgumentAssociation'
	});

	namespace['ArgumentAssociation'].builder.prototype.__isArgumentAssociation = true;
	/**
	 * Define Lambda
	 */

	primitives.defineClass({
		name: 'Lambda'
	});

	namespace['Lambda'].builder.prototype.__isLambda = true;

	/**
	 * Define InterpretedClosure
	 */

	primitives.defineClass({
		name: 'InterpretedClosure'
	});

	namespace['InterpretedClosure'].builder.prototype.__isClosure = true;

	/**
	 * Define JSProxy
	 */

	primitives.defineClass({
		name:       'JSProxy',
		superclass: nil
	});

	namespace['JSProxy'].builder.prototype.__isJSProxy = true;

	/**
	 * Adds the namespace to the namespace
	 * so it can be accessed language side
	 */

	namespace['Ysos'] = namespace;

	/**
	 * Convert primitives into our language
	 */

	primitives.defineMethod({
		special:               true,
		arguments:             ['name', 'arguments', 'body'],
		noArgumentsSeparation: true,
		name:                  'method',
		fn:                    function(name, rawArguments, escapedArray, currentEnvironment) {
			checkArgumentsFormat(rawArguments);
			var args = primitives.extractArguments(rawArguments);

			var compiledMethod = new namespace["CompiledMethod"].builder();
			compiledMethod["@name"] = name;
			compiledMethod["@arguments"] = args;
			compiledMethod["@sourceCode"] = escapedArray;
			compiledMethod["@source"] = window.currentFilePath;

			this.__methodDictionary[name.__content] = compiledMethod;

			primitives.defineMethod({
				arguments: args.map(function(each) {
					return each.__content;
				}),
				name:      name.__content,
				class:     this,
				fn:        function() {
					var i;
					var size;
					var lookupStartingPoint = arguments[arguments.length - 1];
					var env = primitives.environment(currentEnvironment);
					if (args.$__isEscapedArray) {
						size = args.__content.length;
					} else {
						size = args.length
					}

					for (i = 0; i < size; i++) {
						env.variables[ args[i].__content ] = arguments[i];
					}
					env.variables["self"] = this;
					env.variables["super"] = this;

					return interpreter(escapedArray, env, lookupStartingPoint);
				}
			});
		},
		class:                 namespace["Class"]
	});

	primitives.defineMethod({
		arguments:             ['name', 'arguments', 'body'],
		special:               true,
		noArgumentsSeparation: true,
		name:                  'classMethod',
		fn:                    function(name, rawArguments, escapedArray, currentEnvironment) {

			var compiledMethod = new namespace["CompiledMethod"].builder();
			checkArgumentsFormat(rawArguments);
			var args = primitives.extractArguments(rawArguments);
			compiledMethod["@name"] = name;
			compiledMethod["@arguments"] = args;
			compiledMethod["@sourceCode"] = escapedArray;
			this.__classMethodDictionary[name.__content] = compiledMethod;

			primitives.defineMethod({
				arguments: args.map(function(each) {
					return each.__content
				}),
				name:      name.__content,
				class:     this.klass,
				fn:        function() {
					var i;
					var size;

					var env = primitives.environment(currentEnvironment);
					if (args.$__isEscapedArray) {
						size = args.__content.length;
					} else {
						size = args.length
					}

					for (i = 0; i < size; i++) {
						env.variables[args[i].__content] = arguments[i];
					}
					env.variables["self"] = this;
					env.variables["super"] = this;

					return interpreter(escapedArray, env);
				}
			});
		},
		class:                 namespace["Class"]
	});

	primitives.defineMethod({
		special:   true,
		arguments: ['name'],
		name:      'subclass',
		fn:        function(symbol) {
			primitives.defineClass({
				name:       symbol.__content,
				superclass: this
			})
		},
		class:     namespace["Class"]
	});

	primitives.defineMethod({
		arguments: ['name'],
		special:   true,
		name:      'subclass',
		fn:        function(symbol) {
			primitives.defineClass({
				name:       symbol.__content,
				superclass: nil
			})
		},
		class:     namespace["UndefinedObject"]
	});

	primitives.defineMethod({
		arguments: ['value'],
		name:      ':=',
		fn:        function(aLiteral, env) {
			env.assign({
				name:  this.__content,
				value: aLiteral
			});
			return aLiteral;
		},
		class:     namespace["Symbol"]
	});

	primitives.defineMethod({
		arguments: [ 'selector', 'primitiveName'],
		special:   true,
		name:      'primitive',
		class:     namespace["Class"],
		fn:        function(name, selector) {
			primitives.defineMethod({
				arguments: primitivesTable[selector.__content].ysosArguments,
				name:      name.__content,
				class:     this,
				fn:        primitivesTable[selector.__content]
			});
		}
	});

	primitives.defineMethod({
		arguments: [ 'selector', 'primitiveName'],
		special:   true,
		name:      'classPrimitive',
		class:     namespace["Class"],
		fn:        function(name, selector) {
			primitives.defineMethod({
				arguments: primitivesTable[selector.__content],
				name:      name.__content,
				class:     this.klass,
				fn:        primitivesTable[selector.__content]
			});
		}
	});

	primitives.defineMethod({
		special:               true,
		arguments:             [ 'name', 'arguments', 'body' ],
		noArgumentsSeparation: true,
		name:                  'macro',
		fn:                    function(name, rawArguments, escapedArray) {

			var compiledMethod = new namespace["CompiledMethod"].builder();
			checkArgumentsFormat(rawArguments);
			var args = primitives.extractArguments(rawArguments);
			compiledMethod["@name"] = name;
			compiledMethod["@arguments"] = args;
			compiledMethod["@sourceCode"] = escapedArray;

			this.__methodDictionary[name.__content] = compiledMethod;

			primitives.defineMethod({
				arguments: args.map(function(each) {
					return each.__content;
				}),
				name:      name.__content,
				class:     this,
				fn:        function() {
					var i;
					var size;
					var returnedAST;
					var currentEnvironment = arguments[arguments.length - 2];

					var env = primitives.environment();
					if (args.$__isEscapedArray) {
						size = args.__content.length;
					} else {
						size = args.length
					}

					for (i = 0; i < size; i++) {
						env.assign({
							name:  args[i].__content,
							value: arguments[i]});
					}
					env.variables["self"] = this;
					env.variables["super"] = this;

					returnedAST = interpreter(escapedArray, env);
					return interpreter(returnedAST.__content, currentEnvironment);
				}
			});
		},
		class:                 namespace["Class"]
	});

	function checkArgumentsFormat(arguments) {
		var comma = primitives.newSymbol(',');

		arguments.forEach(function(each, index) {
			if(index%2){
				// Odd index
				if(each !== comma){
					throw 'Wrong argument format';
				}
			} else {
				// Even index
				if (each === comma) {
					throw 'Wrong argument format';
				}
			}
		})
	}

});