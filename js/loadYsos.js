define([
	'./interpreter',
	'./parser',
	'./bootstrap',
	'./runtime',
	'jQuery'
], function(interpreter, parser) {

	function loadYsos(spec) {

		var result;
		var pathToKernel = spec.pathToKernel || '';
		var whenLoadedCallback = spec.whenLoadedCallback || function() {
		};

		function forEach(items, fn, onSuccess) {
			recur(Object.create(items), fn, onSuccess);
		}

		// Nico & Ben: It took us some time to write it. It should take you some time to understand it too :)
		function recur(remaining, fn, onSuccess) {
			var head;
			if (remaining.length === 0) {
				onSuccess();
			} else {
				head = remaining[0];
				remaining.splice(0, 1);
				fn(head, function() {
					recur(remaining, fn, onSuccess);
				});
			}
		}

		function loadSources(sources, path, callback, entryPoint) {
			jQuery.get(path + sources, function(sourceFiles) {
				forEach(sourceFiles.split('\n'), function(each, callback) {
					if (entryPoint === false) {
						var split = (path + sources).split('/');
						split.splice(split.length - 1);
						var newPath = split.join('/') + '/';
					}
					computeLine(each, newPath || path, callback);
				}, function() {
					callback(result);
				});

			});
		}

		function computeLine(line, path, callback) {
			var trimmed = line.trim();

			// empty or commented
			if (!trimmed || trimmed[0] === '#') {
				callback();
				return;
			}

			if (trimmed[trimmed.length - 1] === 'p') {
				// point to a meta file
				loadSources(trimmed, path, callback, false);
			} else {
				// points to a source file
				loadFile(trimmed, path, callback);
			}
		}

		function loadFile(source, pathToKernel, callback) {
			jQuery.get(pathToKernel + source, function(sources) {
				window.currentFilePath = (pathToKernel + source);
				result = interpreter(parser.parse(sources));
				callback();
			});
		}

		(function loadSystem() {
			loadSources('kernel.yp', pathToKernel, function() {
				loadSources("sources.yp", '', function(sources) {
					window.currentFilePath = null;
					whenLoadedCallback(sources);
				});
			});
		})();
	}

	return loadYsos;
});
