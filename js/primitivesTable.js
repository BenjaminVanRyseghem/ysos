define([
	'./interpreter',
	'./primitives',
	'./bootstrap'
], function(interpreter, primitives, namespace) {

	var that = {};

	that.Array_add = function(item, env) {
		var result = env.resolve('Array').$new();
		result.__content = Object.create(this.__content);
		result.__content.push(item);

		return result;
	};

	that.Array_add.ysosArguments = ['item'];

	that.Array_addFirst = function(item, env) {
		var result = env.resolve('Array').$new();
		result.__content = Object.create(this.__content);
		result.__content.unshift(item);

		return result;
	};

	that.Array_addFirst.ysosArguments = ['item'];

	that.Array_addAll = function(arrayToAdd, env) {
		var result = env.resolve('Array').$new();
		result.__content = Object.create(this.__content);

		arrayToAdd.__content.forEach(function(item) {
			result.__content.push(item);
		});

		return result;
	};

	that.Array_addAll.ysosArguments = ['arrayToAdd'];

	that.Array_any = function(closure) {
		return this.__content.some(function(each, index) {
			return that.InterpretedClosure_evaluate.apply(closure, [each, index]);
		});
	};

	that.Array_any.ysosArguments = ['closure'];

	that.Array_clone = function(env) {
		var result = env.resolve('Array').$new();
		result.__content = Object.create(this.__content);
		return result;
	};

	that.Array_clone.ysosArguments = [];

	that.Array_detect = function(closure, defaultValue) {
		var found = defaultValue;
		this.__content.forEach(function(each, index) {
			if(that.InterpretedClosure_evaluate.apply(closure, [each, index])){
				found = each;
			}
		});
		return found;
	};

	that.Array_detect.ysosArguments = [ 'closure', 'defaultValue' ];

	that.Array_detectIndex = function(closure, defaultValue) {
		var found = defaultValue;
		this.__content.forEach(function(each, index) {
			if(that.InterpretedClosure_evaluate.apply(closure, [each, index])){
				found = index;
			}
		});
		return found;
	};

	that.Array_detectIndex.ysosArguments = [ 'closure', 'defaultValue' ];

	that.Array_forEach = function(closure, env) {
		var args = closure['@arguments'];
		var body = closure['@body'];

		var result;
		var i;

		for (i = 0; i < this.__content.length; i++) {
			var newEnv = env.clone();
			newEnv.variables[ args.__content[0].__content] = this.__content[i];
			if (args.__content.length > 1) {
				newEnv.variables[ args.__content[1].__content] = i + 1;
			}
			result = interpreter(body.__content, newEnv);
		}

		return result;
	};

	that.Array_forEach.ysosArguments = [ 'closure' ];

	that.Array_fork = function(env) {
		var lambda = this.__content;
		setTimeout(function() {
			interpreter(lambda, env);
		}, 0);
	};

	that.Array_fork.ysosArguments = [];

	that.Array_fromTo = function(from, to, env) {
		var newArray = env.resolve('Array').$new();
		var size = this.__content.length;

		if (from < 0) {
			from = size + from + 1;
		}

		if (to < 0) {
			to = size + to + 1;
		}

		newArray.__content = this.__content.slice(from - 1, to);
		return newArray;
	};

	that.Array_fromTo.ysosArguments = [ 'from', 'to' ];

	that.Array_length = function() {
		return this.length;
	};

	that.Array_length.ysosArguments = [];

	that.Array_reverse = function() {
		var result = new namespace['Array'].builder();
		var length = this.__content.length;
		if (length === 0) {
			return result;
		}
		var array = Object.create(this.__content);
		var left = null;
		var right = null;
		for (left = 0; left < length / 2; left += 1) {
			right = length - 1 - left;
			var temporary = array[left];
			array[left] = array[right];
			array[right] = temporary;
		}
		result.__content = array;
		return result;
	};

	that.Array_reverse.ysosArguments = [];

	that.Boolean_and = function(other) {
		return this && other;
	};

	that.Boolean_and.ysosArguments = [ 'other' ];

	that.Boolean_inspect = function() {
		if (this == true) {
			console.log(true);
		} else {
			console.log(false);
		}
	};

	that.Boolean_inspect.ysosArguments = [];

	that.Boolean_not = function() {
		return !this;
	};

	that.Boolean_not.ysosArguments = [];

	that.Boolean_or = function(other) {
		return this || other;
	};

	that.Boolean_or.ysosArguments = [ 'other' ];

	that.Boolean_thenElse = function(trueEscapedArray, falseEscapedArray, env) {
		if (this == true) {
			return interpreter(trueEscapedArray.__content, env);
		} else {
			return interpreter(falseEscapedArray.__content, env);
		}
	};

	that.Boolean_thenElse.ysosArguments = [ 'trueBlock', 'falseBlock' ];

	that.Class_at = function(selector) {
		return this.__methodDictionary[selector.__content] || namespace['nil'];
	};

	that.Class_at.ysosArguments = [ 'selector' ];

	that.Class_classMethodDictionary = function() {
		var result = new (env.resolve(['Dictionary'])).builder();
		result.__content = this.__classMethodDictionary;

		return result;
	};

	that.Class_classMethodDictionary.ysosArguments = [];

	that.Class_methodDictionary = function() {
		var result = new (env.resolve(['Dictionary'])).builder();
		result.__content = this.__methodDictionary;

		return result;
	};

	that.Class_methodDictionary.ysosArguments = [];

	that.Class_name = function() {
		return this.name;
	};

	that.Class_name.ysosArguments = [];

	that.Class_source = function() {
		return this.__source || namespace['nil'];
	};

	that.Class_source.ysosArguments = [];

	that.Class_subclasses = function(env) {
		var result = new (env.resolve(['Array'])).builder();
		result.__content = this.__subclasses;
		result.__content.sort(function(a, b) {
			return a.name < b.name ? -1 : 1;
		});

		return result;
	};

	that.Class_subclasses.ysosArguments = [];

	that.Dictionary_at = function(key, env) {
		if (this.__isNamespace) {
			return this[key.__content] || env['nil'];
		} else {
			return this['__' + key.__content] || env['nil'];
		}
	};

	that.Dictionary_at.ysosArguments = [ 'key' ];

	that.Dictionary_atPut = function(key, value) {
		if (this.__isNamespace) {
			this[key.__content] = value;
		} else {
			this['__' + key.__content] = value;
		}
		return value;
	};

	that.Dictionary_atPut.ysosArguments = [ 'key', 'value' ];

	that.InterpretedClosure_evaluate = function() {
		var i;
		var args = this['@arguments'];
		var body = this['@body'];

		var env = primitives.environment(this['@environment']);

		for (i = 0; i < args.__content.length; i++) {
			env.assign({
				name:  args.__content[i].__content,
				value: arguments[i]});
		}

		return interpreter(body.__content, env);
	};

	that.InterpretedClosure_evaluate.ysosArguments = function(receiver) {
		return receiver['@arguments'].__content.map(function(each) {
			return each.__content;
		});
	};

	that.InterpretedClosure_whileTrue = function(closure) {
		var result;

		var body = this['@body'];
		var env = primitives.environment(this['@environment']);
		while (interpreter(body.__content, Object.create(env))) {
			result = interpreter(closure['@body'].__content, Object.create(env));
		}

		return result;
	};

	that.InterpretedClosure_whileTrue.ysosArguments = [ 'body' ];

	that.JSFunction_evaluate = function() {
		var result;
		var args = Array.prototype.slice.call(arguments, 0);
		args = args.slice(0, args.length - 2);
		args = convertArgumentsToJS(args);
		result = this.__content.apply(window, args);

		result = primitives.wrapJavascript(result);
		return result;
	};

	that.JSFunction_evaluate.ysosArguments = null;

	that.JSProxy_at = function(key) {
		var result = this.__content[key.__content];
		if (result == undefined) {
			return namespace['nil'];
		}
		return primitives.wrapJavascript(result);
	};
	that.JSProxy_at.ysosArguments = [ 'key' ];

	that.JSProxy_atPut = function(key, value) {
		this.__content[key.__content] = convertArgumentToJS(value);
		return value;
	};
	that.JSProxy_atPut.ysosArguments = [ 'key', 'value' ];

	that.JSProxy_inspect = function() {
		console.log(this.__content);
		return namespace['nil'];
	};

	that.JSProxy_inspect.ysosArguments = [];

	that.JSProxy_messageNotFound = function(aMessageSend) {
		var result;
		var selector = aMessageSend['@selector'];
		var arguments = aMessageSend['@arguments'];
		var args = convertArgumentsToJS(arguments);
		result = this.__content[selector.__content].apply(this.__content, args);
		return primitives.wrapJavascript(result);
	};

	that.JSProxy_messageNotFound.ysosArguments = ['aMessageSend'];

	that.JSProxy_send = function(aMessageSend, currentEnvironment) {
		var selector = aMessageSend['@selector'];
		var arguments = aMessageSend['@arguments'];
		return primitives.sendMessage({
			receiver:           this.__content,
			selector:           selector,
			arguments:          arguments,
			startingPoint:      this.__content.klass,
			currentEnvironment: currentEnvironment,
			jsCall:             true
		});
	};

	that.JSProxy_send.ysosArguments = ['aMessageSend'];

	that.MessageSend_performOn = function(receiver, currentEnvironment) {
		var selector = this['@selector'];
		var arguments = this['@arguments'].__content;
		var superCall = this['@superCall'];
		var startingPoint = receiver.klass;
		if (superCall) {
			startingPoint = startingPoint['@superclass'];
		}

		return primitives.sendMessage({
			receiver:           receiver,
			selector:           selector,
			arguments:          arguments,
			startingPoint:      startingPoint,
			currentEnvironment: currentEnvironment,
			superCall:          superCall
		});
	};

	that.MessageSend_performOn.ysosArguments = [ 'receiver' ];

	that.Number_divides = function(other) {
		return this / other;
	};

	that.Number_divides.ysosArguments = [ 'other' ];

	that.Number_egality = function(other) {
		return (this.builder === other.builder) && (this == other);
	};
	that.Number_egality.ysosArguments = [ 'other' ];

	that.Number_inegality = function(other) {
		return (this.builder !== other.builder) || (this != other);
	};
	that.Number_inegality.ysosArguments = [ 'other' ];


	that.Number_gt = function(other) {
		return this > other;
	};

	that.Number_gt.ysosArguments = [ 'other' ];

	that.Number_gte = function(other) {
		return this >= other;
	};

	that.Number_gte.ysosArguments = [ 'other' ];

	that.Number_lt = function(other) {
		return this < other;
	};

	that.Number_lt.ysosArguments = [ 'other' ];

	that.Number_lte = function(other) {
		return this <= other;
	};

	that.Number_lte.ysosArguments = [ 'other' ];

	that.Number_minus = function(other) {
		return this - other;
	};

	that.Number_minus.ysosArguments = [ 'other' ];

	that.Number_plus = function(other) {
		return this + other;
	};

	that.Number_plus.ysosArguments = [ 'other' ];

	that.Number_inspect = function() {
		console.log(Number(this));
	};

	that.Number_inspect.ysosArguments = [];

	that.Number_times = function(other) {
		return this * other;
	};

	that.Number_times.ysosArguments = [ 'other' ];

	that.Number_toDoBy = function(limit, by, args, escapedArray, env) {
		var result;
		var i;
		var newEnv = Object.create(env);

		for (i = this; i <= limit; i += by) {
			newEnv.variables[ args.__content[0].__content] = i;
			result = interpreter(escapedArray.__content, newEnv);
		}

		return result;
	};

	that.Number_toDoBy.ysosArguments = [ 'limit', 'by', 'arguments', 'body' ];

	that.Number_toString = function() {
		return this.toString();
	};

	that.Number_toString.ysosArguments = [];

	that.Object_class = function() {
		return this.klass;
	};

	that.Object_class.ysosArguments = [];

	that.Object_egality = function(other) {
		return this === other;
	};

	that.Object_egality.ysosArguments = [ 'other' ];

	that.Object_inegality = function(other) {
		return this !== other;
	};

	that.Object_inegality.ysosArguments = [ 'other' ];

	that.Object_inspect = function() {
		console.log(this);
	};

	that.Object_inspect.ysosArguments = [];

	that.Object_mnu = function(aMessageSend) {
		throw 'Message not found: ' + aMessageSend.$selector();
	};

	that.Object_mnu.ysosArguments = [ 'aMessageSend' ];

	that.Object_toString = function() {
		var prefix;
		var name = this.klass.name;
		var voyels = ['a', 'e', 'i' , 'o' , 'u', 'y'];
		if ((voyels.indexOf(name[0].toLowerCase()) !== -1)) {
			prefix = 'an';
		} else {
			prefix = 'a';
		}
		return prefix + ' ' + this.klass.name;
	};

	that.Object_toString.ysosArguments = [];

	that.String_addToString = function(aString) {
		return aString + this;
	};

	that.String_addToString.ysosArguments = [ 'aString' ];

	that.String_asRegexp = function(options) {
		return new RegExp(this, options);
	};

	that.String_asRegexp.ysosArguments = [ 'options' ];

	that.String_asSymbol = function() {
		return primitives.newSymbol(this);
	};

	that.String_asSymbol.ysosArguments = [];

	that.String_egality = function(other) {
		return String(this) === other;
	};

	that.String_egality.ysosArguments = [ 'other' ];

	that.String_fromTo = function(from, to) {
		var size = this.length;

		if (from < 0) {
			from = size + from + 1;
		}

		if (to < 0) {
			to = size + to + 1;
		}

		return this.substring(from - 1, to);
	};

	that.String_fromTo.ysosArguments = [ 'from', 'to'];

	that.String_inspect = function() {
		console.log(String(this));
	};

	that.String_indexOfStartingAt = function(item, startingPoint) {
		return this.indexOf(item, startingPoint - 1) + 1;
	};

	that.String_indexOfStartingAt.ysosArguments = [ 'item', 'startingPoint'];

	that.String_inspect.ysosArguments = [];

	that.String_lastIndexOf = function(aString) {
		return this.lastIndexOf(aString) + 1;
	};

	that.String_lastIndexOf.ysosArguments = [ 'aString' ];

	that.String_length = function() {
		return this.length;
	};

	that.String_length.ysosArguments = [];

	that.String_split = function(separator) {
		return this.split(separator);
	};

	that.String_split.ysosArguments = [ 'separator' ];

	that.String_throw = function() {
		throw this;
	};

	that.String_throw.ysosArguments = [];

	that.Symbol_addToString = function(aString) {
		return aString + this.__content;
	};

	that.Symbol_addToString.ysosArguments = [ 'aString' ];

	that.Symbol_alter = function(anEscapedArray, env) {
		var newEnv = env.clone();

		if (this.isBeingAltered) {
			throw 'You are altering #' + this.__content + ' during its alteration. This leads to an infinite loop';
		}

		// To avoid side effect if a parallel process mutates the environment
		this.isBeingAltered = true;
		var newValue = interpreter(anEscapedArray.__content, newEnv);
		this.isBeingAltered = false;

		// the environment did not mutate since the beginning
		if (newEnv.equal(env)) {
			env.forceAssign({
				name:  this.__content,
				value: newValue
			});
			return newValue;
		}

		// if the environment mutates, let's try again
		return that.Symbol_alter(anEscapedArray, env);
	};

	that.Symbol_alter.ysosArguments = [ 'body' ];

	that.Symbol_new = function(content) {
		return primitives.newSymbol(content);
	};

	that.Symbol_new.ysosArguments = [ 'content' ];

	function convertArgumentToJS(argument) {
		if (argument.__isJSProxy) {
			return argument.__content;
		} else if (argument.__isClosure) {
			return function() {
				var args = Array.prototype.slice.call(arguments, 0);
				args = args.map(function(each) {
					return primitives.wrapJavascript(each);
				});
				return that.InterpretedClosure_evaluate.apply(argument, args);
			};
		} else if (argument.$__isEscapedArray) {
			return convertArgumentsToJS(argument.__content);
		} else if (argument.$__isSymbol) {
			if (argument[0] === '#') {
				return argument.__content.substr(1, argument.__content.length);
			} else {
				return argument.__content;
			}
		} else if (argument.constructor === Array && argument.length === 0) {
			return [];
		} else if (argument.klass.name === 'Dictionary') {
			var dict = {};
			for (var key in argument) {
				if (key[0] === '_' && key[1] === '_') {
					var newKey = key.substr(2, key.length);
					dict[newKey] = convertArgumentsToJS([argument[key]])[0];
				}
			}
			return dict;
		} else if (argument.klass.name === 'String') {
			return String(argument);
		} else {
			return argument;
		}
	}

	function convertArgumentsToJS(args) {
		return args.map(function(each) {
			return convertArgumentToJS(each);
		});
	}

	return that;
});